#ifndef _MAPLE_H
#define _MAPLE_H

void maple_print_lincomb(ivlincomb *ht, char *letter, int nz);

void maple_qprint_lincomb(ivlincomb *lc, int level, char *letter);

#endif
