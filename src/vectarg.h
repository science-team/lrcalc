#ifndef _VECTARG_H
#define _VECTARG_H

#include "ivector.h"

ivector *get_vect_arg(int ac, char **av);

#endif
