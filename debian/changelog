lrcalc (2.1-2) experimental; urgency=medium

  * Update debian/copyright, license is now GPL-3+.

 -- Tobias Hansen <thansen@debian.org>  Sun, 24 Apr 2022 19:04:19 +0100

lrcalc (2.1-1) experimental; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Fix Maintainer name of Debian Science team (routine-update)
  * Point Vcs fields to salsa.debian.org (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Update watch file format version to 4.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Submit (from ./configure).
  * Avoid explicitly specifying -Wl,--as-needed linker flag.
  * Apply multi-arch hints.
    + liblrcalc-dev: Add Multi-Arch: same.
  * Remove debian/gbp.conf
  * Use d-shlibs to be sure SOVERSION bumps are properly done
  * Enable static library
  * Enable dh_missing --fail-missing

  [ Tobias Hansen ]
  * Move package to Debian Math Team.

 -- Tobias Hansen <thansen@debian.org>  Sun, 24 Apr 2022 14:06:09 +0000

lrcalc (1.2-2) unstable; urgency=medium

  * New patch debian/includes.patch:
    - Fix include syntax in headers. (Closes: #819989)
  * Bump Standards-Version to 3.9.8.
  * Update Vcs-* fields.

 -- Tobias Hansen <thansen@debian.org>  Mon, 01 Aug 2016 16:20:23 +0000

lrcalc (1.2-1) unstable; urgency=low

  * New upstream release.
  * Also install the schubmult program.
  * Change Section from math to libs for liblrcalc1.
  * Bump Standards-Version to 3.9.5.
  * Update watch file.

 -- Tobias Hansen <thansen@debian.org>  Tue, 05 Aug 2014 22:44:54 +0200

lrcalc (1.1.7-1) unstable; urgency=low

  * Initial release (Closes: #703736).

 -- Tobias Hansen <thansen@debian.org>  Mon, 26 Aug 2013 21:38:03 +0200
